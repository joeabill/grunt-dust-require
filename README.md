Grunt.js plugin to compile dustjs templates and wrap them in requirejs modules with all partial templates used as dependencies.

## Getting Started
This plugin requires Grunt `~0.4.0`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-dust-require --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-dust-require');
```

## Dust task
_Run this task with the `grunt grunt-dust-require` command._

Task targets, files and options may be specified according to the grunt [Configuring tasks](http://gruntjs.com/configuring-tasks) guide.

### File Options

#### amdPrefix
Type: `string`
Default: null

Sets a prefix for the requirejs module name. `amdPrefix/path/to/template`.

### Options

#### relative
Type: `boolean`
Default: false

Make templates names relative from cwd (working only if used [Grunt Dynamic Mappings](http://gruntjs.com/configuring-tasks#building-the-files-object-dynamically)).

#### basePath
Type: `string`
Default: false

Exclude this path from templates names.

#### useBaseName
Type: `boolean`
Default: false

If 'true' template names will be the same as the basename of the file, sans prepended paths and file extensions. When coupled with globbing pattern '[root_folder]/**/*' all files matched will use their base names regardless of where the file is located in the directory tree rooted at root_folder. Note: One caveat - filenames must be unique! Otherwise name collisions will occur.



### Usage Examples

```coffee
grunt.initConfig
  "dust-require":
    compile:
      files: [
          {
            expand: true
            outputStyle: 'compressed'
            cwd: "exampleFolder"
            src: ['**/*.dust']
            dest: "compiledFolder"
            ext: '.js'
            amdPrefix: "templates/"
            nameTemplate: false
          }
        ]
```

example.dust
```dust
<div class='example'>
  {> "path/to/partial/template" someVar="{someOtherVar}" /}
  {> "path/to/another/partial" /}
  {> "another/path/omg" anotherVar="someValue" /}
</div>
```

example.js
```js
define("templates/example", ["templates/path/to/partial/template", "templates/path/to/another/partial", "templates/another/path/omg"], function() {
  // example.dust
  (function() {
    dust.register("example", body_0);

    function body_0(chk, ctx) {
      return chk.write("<div class='example'>").partial("path/to/partial/template", ctx, {
        "someVar": body_1
      }).partial("path/to/another/partial", ctx, null).partial("another/path/omg", ctx, {
        "anotherVar": "someValue"
      }).write("</div>");
    }

    function body_1(chk, ctx) {
      return chk.reference(ctx._get(false, ["someOtherVar"]), ctx, "h");
    }
    return body_0;
  })();
  return
});
```




For more examples on how to use the `expand` API to manipulate the default dynamic path construction in the `glob_to_multiple` examples, see "Building the files object dynamically" in the grunt wiki entry [Configuring Tasks](http://gruntjs.com/configuring-tasks).

## Release History
* v0.1.0
  - First release

## License
Copyright (c) 2013 Marco Salazar
Licensed under the MIT license.




[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/salazarm/grunt-dust-require/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

