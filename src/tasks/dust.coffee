###
# grunt-dust-require
# https://github.com/salazarm/grunt-dust-require
# Copyright (c) 2014 Marco Salazar
# Licensed under the MIT license.
###

TEMPLATE_NAME_REGEX = /\{\s*>\s*"([^"]*)".*?\}/g

module.exports = ( grunt ) ->

	_ = grunt.util._
	dust = require "dustjs-linkedin"
	path = require "path"
	fs = require "fs"
	amdHelper = require( "../helpers/amd" ).init grunt

	grunt.registerMultiTask "dust-require", "Task to compile dustjs templates.", ->
		options = @options
			basePath: no
			useBaseName: no
			relative: no

		for file in @files
			output = []
			templateNames = []
			subtemplates = []

			for source in file.src
				# relative path to
				tplRelativePath =
					if file.orig.cwd? and options.relative
						# for dynamic_mappings task
						path.relative(file.orig.cwd, source).split(path.sep).join('/')
					else if options.basePath
						# for basePath option
						path.relative(options.basePath, source).split(path.sep).join('/')
					else
						source

				prefix = file.amdPrefix || ""

				tplName =
          if options.useBaseName
            # use basename as template name
            path.basename tplRelativePath, path.extname tplRelativePath
          else
            # remove extension from template name
            if file.orig.cwd?
            	path.relative(file.orig.cwd, source).replace new RegExp( "\\#{ path.extname tplRelativePath }$" ), ""
            else
	            tplRelativePath.replace new RegExp( "\\#{ path.extname tplRelativePath }$" ), ""

	      tplName = prefix + tplName
	      tplName = tplName.replace new RegExp(  "\\\\", "g" ), "/"
	      readSource = grunt.file.read( source )
	      while (m=TEMPLATE_NAME_REGEX.exec(readSource))
	      	subtemplates.push(prefix+m[1])

				try
					if file.orig.cwd?
						name = path.relative(file.orig.cwd, source)
						name = name.replace new RegExp(  "\\\\", "g" ), "/"
						output.push "// #{ name }\n" + dust.compile readSource, name.split( path.extname name )[0]
					else
						output.push "// #{ tplRelativePath }\n" + dust.compile readSource( source ), tplName
					templateNames.push tplName
				catch e
					# Handle error and log it with Grunt.js API
					grunt.log.error().writeln e.toString()
					grunt.warn "DustJS found errors.", 10


			subtemplates = _.uniq subtemplates
			if templateNames.length > 1
				subtemplatesMap = {}
				for temp in subtemplates
					subtemplatesMap[temp] = true
				for temp in templateNames
					delete subtemplatesMap[temp]
				subtemplates = _.keys subtemplatesMap

			if output.length > 0
				joined = output.join("\n ")
				switch templateNames.length
					when 0
						returning = undefined
					when 1
						returning = _.last templateNames
					else
						returning = templateNames
				joined = amdHelper joined, returning, subtemplates

			grunt.file.write file.dest, joined
			grunt.log.writeln("File "+file.dest+ (" created").yellow.bold )