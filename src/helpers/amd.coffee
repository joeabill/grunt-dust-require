beautify = require( "js-beautify" ).js_beautify

module.exports.init = (grunt) ->
	(content, returning, subtemplates) ->
		args = []
		paths = []
		parts = []

		# package name
		if typeof returning is "string"
			parts.push JSON.stringify returning
		parts.push JSON.stringify subtemplates if subtemplates.length

		if typeof returning is "string"
			# single template
			amdCallback = """
				function (#{ args.join "," }) {
					#{ content }
					return
				}
			"""
		else
			# bunch of templates
			defines = for item in returning
				"""
					define(#{ JSON.stringify item }, function() {
						return #{ renderFunction.replace "<%= template_name %>", JSON.stringify item }
					});
				"""
			amdCallback = """
				function (#{ args.join "," }) {
					#{ content }
					#{ defines.join "" }
					return #{ JSON.stringify returning };
				}
			"""
			
		parts.push amdCallback
		beautify "define(#{ parts.join( "," ) });", indent_size: 2
